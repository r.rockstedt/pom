from gurobipy import GRB, Model, quicksum
# item sizes a, item profits p, capacity b


def solve(a, p, b):
    my_model = Model("knapsackmodel")
    # add vars
    x = {}
    for i in range(len(p)):
        x[i] = my_model.addVar(vtype='c', obj=p[i])

    # add constraints
    my_model.addConstr(quicksum(a[i] * x[i] for i in range(len(a))) <= b, name="sacksize")
    my_model.ModelSense = GRB.MAXIMIZE
    my_model.optimize()

