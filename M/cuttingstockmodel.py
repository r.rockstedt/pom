from gurobipy import GRB, Model, quicksum

def solve(m, L, d, l):
    my_model = Model("makespan")
    # add vars
    n = len(l)
    x = my_model.addVars(n, m, vtype=GRB.INTEGER)
    y = my_model.addVars(m, vtype=GRB.BINARY)
    # add Constr
    my_model.addConstrs(quicksum(x[i, j] for j in range(m)) == d[i] for i in range(n))
    my_model.addConstrs((quicksum(l[i] * x[i, j] for i in range(n)) <= L for j in range(m)), name="roll_length")
    my_model.addConstrs(x[i, j] <= d[i] * y[j] for i in range(n) for j in range(m))
    my_model.setObjective(quicksum(y[j] for j in range(m)), GRB.MINIMIZE)
    # Relax to LP
    #my_model.relax()
    my_model.optimize()
