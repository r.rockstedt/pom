from gurobipy import *

def solve(m, L, d, l):
    model = Model("csflow")
    i = 0
    while i < len(l):
        j = 0
        while j < len(l):
            if l[i] == l[j] and i != j:
                d[i] += d[j]
                del d[j]
                del l[j]
                j -= 1
            j += 1
        i += 1

    a_2 = [(i,i+1) for i in range(L+1)]

    a_1 = [[(j, j+l[i]) for j in range(1, L+2-l[i])] for i in range(len(l))]
    
    e_1 = []
    for i in range(len(l)):
        e_1 += a_1[i]

    e_tl = tuplelist(list(dict.fromkeys(e_1 + a_2)))

    x = {}
    for i in range(L+1):
        x[i] = {}
    for e in e_tl:
        x[e[0]][e[1]] = model.addVar(vtype=GRB.INTEGER, name=f"x_{e[0]}_{e[1]}")

    for i in range(1,L+1):
        outgoing = quicksum(x[e[0]][e[1]] for e in e_tl.select(i, '*'))
        incoming = quicksum(x[e[0]][e[1]] for e in e_tl.select('*', i))
        model.addConstr(outgoing - incoming == 0)

    for i in range(len(l)):
        model.addConstr(quicksum(x[a[0]][a[1]] for a in a_1[i]) >= d[i])

    model.setObjective(x[0][1], GRB.MINIMIZE)
    model.optimize()
    print(x[0][1].X)
    
    lp = model.relax()
    lp.optimize()
    print(x[0][1].X)

    #for v in model.getVars():
    #    print(f"{v.VarName} = {v.X}")