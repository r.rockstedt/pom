import matplotlib.pyplot as plt
import networkx as nx

def solve(n, E, s,t ):
    G = nx.DiGraph()
    for i in range(n):
        G.add_node(i)
    G.add_weighted_edges_from(E)
    solution = nx.dijkstra_path_length(G, s, t)
    print(solution)

def solve_kn(a, p, b):
    G = nx.DiGraph()
    b = b+1
    for i in range(len(a)):
        for c in range(b):
            G.add_node((c, i))

    for i in range(len(a)):
        if i != 0:
            #Add skip arcs
            for c in range(b):
                if c < b-a[i]:
                    # Add item arcs
                    G.add_edge((c, i - 1), (c + a[i], i), weight=p[i])
                # Add skip  arcs
                G.add_edge((c, i - 1), (c, i), weight=0)
                if c < (b-1):
                    # Add waste arcs
                    G.add_edge((c, i), (c + 1, i), weight=0)

    solution = nx.dag_longest_path_length(G)
    print(solution)

