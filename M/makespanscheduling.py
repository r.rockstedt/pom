from gurobipy import GRB, Model, quicksum

def solve(m, p, b):
    my_model = Model("makespan")
    # add vars
    n = len(p)
    x = my_model.addVars(n, m, vtype=GRB.BINARY)
    c_max = my_model.addVar(vtype=GRB.INTEGER)
    # add Constr
    my_model.addConstrs(quicksum(x[j, k] for k in range(m)) == 1 for j in range(n))
    my_model.addConstrs((quicksum(p[j] * x[j, k] for j in range(n)) <= c_max) for k in range(m))
    my_model.addConstr(c_max >= 0)

    #extra constraint
    my_model.addConstr(69 <= c_max)
    my_model.addConstr(c_max <= 70)

    my_model.setObjective(c_max, GRB.MINIMIZE)
    # my_model.ModelSense = GRB.MINIMIZE
    my_model.optimize()
