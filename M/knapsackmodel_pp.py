from pyscipopt import Model, quicksum
# item sizes a, item profits p, capacity b


def solve(a, p, b):
    my_model = Model("knapsackmodel")

    # add vars
    x = {}
    for i in range(len(a)):
        x[i] = my_model.addVar(vtype="b", obj=p[i])

    # add contrs
    # add constraints
    my_model.addCons(quicksum(a[i] * x[i] for i in range(len(a))) <= b, name="sacksize")
    my_model.setMaximize()
    my_model.optimize()
    print(my_model.getBestSol())

