from gurobipy import GRB, Model, quicksum

def solve(m, a, b):
    my_model = Model("binbacking")
    n = len(a)
    x = my_model.addVars(n, m, vtype=GRB.BINARY)
    y = my_model.addVars(m, vtype=GRB.BINARY)
    # Add constr
    my_model.addConstrs(quicksum(x[i, j] for j in range(m)) == 1 for i in range(n))
    my_model.addConstrs(quicksum(a[i] * x[i, j] for i in range(n)) <= b * y[j] for j in range(m))

    #extra constraint
    #my_model.addConstrs(x[i, j] <= y[j] for j in range(m) for i in range(n))

    my_model.setObjective(quicksum(y[j] for j in range(m)), GRB.MINIMIZE)
    my_model.optimize()



# item sizes a, item profits p, capacity b


