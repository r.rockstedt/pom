from gurobipy import GRB, Model, quicksum
# item sizes a, item profits p, capacity b


def solve(a, p, b, C):
    my_model = Model("knapsackmodel")
    # add vars
    x = {}
    for i in range(len(p)):
        x[i] = my_model.addVar(vtype='b', obj=p[i])

    # add constraints
    my_model.addConstr(quicksum(a[i] * x[i] for i in range(len(a))) <= b, name="sacksize")
    for c in C:
        i = c[0]
        j = c[1]
        my_model.addConstr((x[i] + x[j] <= 1))
    my_model.ModelSense = GRB.MAXIMIZE
    my_model.optimize()


